<?php
  
  class Api_model extends CI_Model {
      
      function exist_user_name($username) {
          
          $query = $this->db->get_where('tb_user', array('user_name' => $username));
          if ($query->num_rows() == 0){
                return false;
          } else {
                return true;
          }
      }
      
      function add_user($data) {
          
          $this->db->insert('tb_user', $data);
          return $this->db->insert_id();
      }
      
      public function login($data){
            $query = $this->db->get_where('tb_user', array('user_name' => $data['user_name']));
            if ($query->num_rows() == 0){
                return false;
            }
            else{
                //Compare the password attempt with the password we have stored.
                $result = $query->row_array();
                $validPassword = password_verify($data['password'], $result['password']);
                if($validPassword){
                    return $result = $query->row_array();
                }                
            }
      }
              
      function createProperty($data) {
          
          $this->db->insert('tb_property', $data);
          return $this->db->insert_id();           
      }
      
      function editProperty($id, $data) {
          $this->db->where('id', $id);
          $this->db->update('tb_property', $data); 
      }
      
      function deleteProperty($id) {
          
          $this->db->where('id', $id);
          $this->db->delete('tb_property');
      }
      
      function soldProperty($id) {
          
          $this->db->where('id', $id);
          $this->db->set('status', 'sold');
          $this->db->update('tb_property');
      }
      
      function getAllPropertyList() {
          
          return $this->db->select('A.user_name, A.email, A.phone, A.photo_url,
                            B.*')
                   ->from('tb_property as B')
                   ->join('tb_user as A', 'A.id = B.user_id')
                   ->get()
                   ->result_array();
          
      }
      
      function updateEmail($user_id, $email) {
          $this->db->where('id', $user_id);
          $this->db->set('email', $email);
          $this->db->update('tb_user');
      }
      
      function updatePhone($user_id, $phone) {
          $this->db->where('id', $user_id);
          $this->db->set('phone', $phone);
          $this->db->update('tb_user');
      }
      
      function addShortList($data) {
          
          $query = $this->db->get_where('tb_shortlist', array('user_id'=>$data['user_id'], 'property_id'=>$data['property_id']));
          
          if ($query->num_rows() == 0) {
              
              $this->db->insert('tb_shortlist', $data);
              $this->db->insert_id();
              
          }           
      }
      
      function getPropertyList($user_id) {
          
          return $this->db->get_where('tb_property', array('user_id'=>$user_id))->result_array();
          
      }
      
      function getShortList($user_id) {          
         
          $this->db->select('tb_property.*, B.user_name, B.email, B.phone, B.photo_url');
          $this->db->from('tb_property');
          $this->db->where('tb_property.user_id', $user_id);
          $this->db->join('tb_shortlist', 'tb_property.id = tb_shortlist.property_id');
          $this->db->join('tb_user as B', 'B.id = tb_property.user_id');

          return $this->db->get()->result_array();
          
      }
      
      function upload_photo($user_id, $data) {
        
          $this->db->where('id', $user_id); 
          $this->db->update('tb_user', $data);
          return true;
      }
      
      function resetPropertyList($user_id) {
          
          $this->db->where('user_id', $user_id);
          $this->db->delete('tb_property');
      }
      
      function removeShortList($data) {
          
          $this->db->delete('tb_shortlist', $data);
      }
      
      function checkEmail($user_id, $email) {
          
          $this->db->where('id', $user_id);
          $this->db->where('email', $email);
          if ($this->db->get('tb_user')->num_rows() > 0) {
              return true;
          } else {
              return false;
          }
      }
      
      function resetPassword($user_id, $old_pwd, $new_pwd) {
          
          $query = $this->db->get_where('tb_user', array('id' => $user_id));
          if ($query->num_rows() == 0){              
              return false;
          } else {
              //Compare the password attempt with the password we have stored.
              $result = $query->row_array();
              $validPassword = password_verify($old_pwd, $result['password']);
              if($validPassword){
                    
                  $this->db->where('id', $user_id);
                  $this->db->set('password', password_hash($new_pwd, PASSWORD_BCRYPT));
                  $this->db->update('tb_user');
                  return true;
              } else {
                  return false;
              }                
          }
          
      }
      
      function searchProperty($data) {
          
          $this->db->select('A.*, 
                            B.user_name, 
                            B.email, 
                            B.phone, 
                            B.photo_url');
          $this->db-> from ('tb_property as A');
          
          if ($data['area'] != "") {
              $this->db->where('A.area', $data['area']);              
          }
          if ($data['keyword'] != "") {
              $this->db->like('A.title', $data['keyword']);              
          }
          if ($data['sale_type'] != "") {
              $this->db->where('A.sale_type', $data['sale_type']);              
          }
          if ($data['listing_type'] != "") {
              $this->db->where('A.listing_type', $data['listing_type']);              
          }
          if ($data['acquistition_type'] != "") {
              $this->db->where('A.acquistition_type', $data['acquistition_type']);              
          }
          if ($data['min_price'] != "") {
              $this->db->where('A.price >=', $data['min_price']);              
          }
          if ($data['max_price'] != "") {
              $this->db->where('A.price <=', $data['max_price']);              
          }
          if ($data['bed_count'] != "") {
              $this->db->where('A.bed_count', $data['bed_count']);              
          }
          if ($data['bath_count'] != "") {
              $this->db->where('A.bath_count', $data['bath_count']);              
          }
          $this->db->join('tb_user as B', 'B.id = A.user_id');
          return $this->db->get()->result_array();     
          //return $this->db->get('tb_property')->result_array();   
          
              
          
      } 
      
      function getPropertyOwnerInfo($property_id) {
          
          return $this->db->select('A.user_name, B.title')
                    ->from('tb_property as B')
                    ->where('B.id', $property_id)
                    ->join('tb_user as A', 'A.id = B.user_id')
                    ->get()
                    ->row_array();          
      }
      
      function addNotification($data) {
          $this->db->insert('tb_notification', $data);
      }
      
      function getevent() {         
          
          $this->db->order_by('created_at', 'DESC');
          $this->db->limit(20, 0);
          return $this->db->get('tb_notification')->result_array();
      }                         
      
  }
?>
