<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//include("./vendor/autoload.php");

class Api extends CI_Controller {

    

    function __construct(){

        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('app/api_model', 'api_model');
        $this->load->library('session');     
    }
    
    /**
     * Make json response to the client with result code message
     *
     * @param p_result_code : Result code
     * @param p_result_msg : Result message
     * @param p_result : Result json object
     */

    private function doRespond($p_result_code,  $p_result){

         $p_result['result_code'] = $p_result_code;

         $this->output->set_content_type('application/json')->set_output(json_encode($p_result));
    }

    /**
     * Make json response to the client with success.
     * (result_code = 0, result_msg = "success")
     *
     * @param p_result : Result json object
     */

    private function doRespondSuccess($result){

        $result['message'] = "Success.";
        $this->doRespond(200, $result);
    }

    /**
     * Url decode.
     *
     * @param p_text : Data to decode
     *
     * @return text : Decoded text
     */

    private function doUrlDecode($p_text){

        $p_text = urldecode($p_text);
        $p_text = str_replace('&#40;', '(', $p_text);
        $p_text = str_replace('&#41;', ')', $p_text);
        $p_text = str_replace('%40', '@', $p_text);
        $p_text = str_replace('%20',' ', $p_text);
        $p_text = trim($p_text);


        return $p_text;
    }
    
    function version() {
        phpinfo();
    }
    
    /**
    * Send Firebase push notification
    * 
    * @param mixed $user_id
    * @param mixed $type
    * @param mixed $body
    * @param mixed $content
    */
    function sendPush($user_id, $type, $body, $content) {
        
        // send FCM push notification
        
        $url = "https://fcm.googleapis.com/fcm/send";
        $api_key = "AAAAPSde5aY:APA91bHgm1XpjkX3HoxLToc4KgP7qEKu4cRf2f40hVnOKM02bb_-s0yfiWTMz1K9_zJNNj5VeVQ06QmsONNruudhIeokdIyMHsSpxZYHBz-cslstDXLT6dh3ETQNQu8uEHtgCg5oJM6o";
        
        $token = "";
        $token = $this->api_model->getToken($user_id);
        
        if (strlen($token) == 0 ) {

            return;
        }
        
        /*
        if(is_array($target)){
            $fields['registration_ids'] = $target;
        } else{
            $fields['to'] = $target;
        }

        // Tpoic parameter usage
        $fields = array
                    (
                        'to'  => '/topics/alerts',
                        'notification'          => $msg
                    );
        $data = array('msgType' => $type,
                      'content' => $content);
        */
        $msg = array
                (
                    'body'     => $body,
                    'title'    => 'WIPR',   
                    'badge' => 1,             
                    'sound' => 'default'/*Default sound*/
                );
                
        $data = array('msgType' => "notification",
                      'content' => $content);
                      
        $fields = array
            (
                //'registration_ids'    => $tokens,
                'to'                => $token,
                'notification'      => $msg,
                'priority'          => 'high',
                'data'              => $data
            );

        $headers = array(
            'Authorization: key=' . $api_key,
            'Content-Type: application/json'
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);

        //@curl_exec($ch);
        
        $result['result'] = curl_exec($ch); 

        curl_close($ch); 
        
        return $result;
        
    }
    
    public function sendMail( $email, $code){              
             
            $to = $email;
            $subject = "Welcome to Peek.\n\n";
            $message = "Your verification code is : \n"
                        .$code.".\n\n" ;
                       
            $from = APP_NAME;
            $headers = "Mime-Version:1.0\n";
            $headers .= "Content-Type : text/html;charset=UTF-8\n";
            $headers .= "From:" . $from;           
            
            return mail($to, $subject, $message, $headers);         
    }
    
    public function makeRandomCode(){
         
         mt_srand();

         $random_code = '';

         $arr = array('1','2','3','4','5','6','7','8','9','0');

         for ($i = 0 ; $i < 6 ; $i++) {

             $index = mt_rand(0, 9);
             $random_code .= $arr[$index];
         }

         return $random_code;
     }     
    
    function stripe_payment() {
        
        $result = array();
        
        $stripe_token = $_POST['stripe_token'];
        $email = $_POST['email'];
        $amount = $_POST['amount'];
        
        try {
            
            \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);

            $message = \Stripe\Charge::create(array(
              "amount" => $amount,
              "currency" => "usd",
              "source" => $stripe_token, // obtained with Stripe.js
              "description" => "Paid from ".$email
            ));
            
            $result = $message;
            $this->doRespondSuccess($result);
            
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();//`enter code here`
            $err = $body['error'];
            $this->doRespond(PAYMENT_ERROR, $err);
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err = $body['error'];
            $this->doRespond(PAYMENT_ERROR, $err);
            //return $err['message'];
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $body = $e->getJsonBody();
            $err = $body['error'];
            //return $err['message'];
            $this->doRespond(PAYMENT_ERROR, $err);
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $body = $e->getJsonBody();
            $err = $body['error'];
            //return $err['message'];
            $this->doRespond(PAYMENT_ERROR, $err);
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $body = $e->getJsonBody();
            $err = $body['error'];
            //return $err['message'];
            $this->doRespond(PAYMENT_ERROR, $err);
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $body = $e->getJsonBody();
            $err = $body['error'];
            //return $err['message'];
            $this->doRespond(PAYMENT_ERROR, $err);
        }        
    }    
    
    /**
    * get user object from query array result
    * 
    * @param mixed $user_array query result array
    */
    function getUser($user_array) {
        
        $user_object = array('user_id' => $user_array['id'],                             
                             'user_name' => $user_array['user_name'],                              
                             'user_type' => $user_array['user_type'],                              
                             'email' => $user_array['email'],                              
                             'phone' => $user_array['phone'],                              
                             'photo_url' => $user_array['photo_url'],                              
                             'created_at' => $user_array['created_at']
                             );
        return $user_object;
    }

    function signup() {
        
        $result = array();
        
        $user_name = $_POST['user_name']; 
        $user_type = $_POST['user_type'];
        $password = $_POST['password'];
        
        if ($this->api_model->exist_user_name($user_name)) {
             $result['message'] = "user name already exist.";
             $this->doRespond(201, $result);             
        } else {
            $data = array('user_name' => $user_name,                          
                          'user_type' => $user_type,
                          'password' => password_hash($password, PASSWORD_BCRYPT),
                          'created_at' => date('Y-m-d h:m:s'),
                          'updated_at' => date('Y-m-d h:m:s')
                          );
            $result["user_id"] = $this->api_model->add_user($data);            
            $this->doRespondSuccess($result);  
        }
    }    
    
    function signin() {
        
        $result = array();
        
        $user_name = $_POST['user_name'];
        $password = $_POST['password'];
            
        $data = array(
            'user_name' => $user_name,
            'password' => $password
        );
        $q_result = $this->api_model->login($data);
        if ($q_result == TRUE) {
            $result['user_model'] = $this->getUser($q_result);
            $this->doRespondSuccess($result);            
        } else{
            $result['message'] = 'Invalid user name or Password!';
            $this->doRespond(202, $result);
        }        
    }    
    
    function createProperty() {
        
        $result = array();
        
        $user_id = $_POST['user_id'];
        $address = $_POST['address'];
        $area = $_POST['area'];
        $title = $_POST['title'];
        $sale_type = $_POST['sale_type'];
        $listing_type = $_POST['listing_type'];
        $acquistition_type = $_POST['acquistition_type'];
        $bed_count = $_POST['bed_count'];
        $bath_count = $_POST['bath_count'];
        $price = $_POST['price'];
        $description = $_POST['description'];
                
        if(!is_dir("uploadfiles/")) {
            mkdir("uploadfiles/");
        }
        $upload_path = "uploadfiles/";  

        $cur_time = time();
         
        $dateY = date("Y", $cur_time);
        $dateM = date("m", $cur_time);
         
        if(!is_dir($upload_path."/".$dateY)){
            mkdir($upload_path."/".$dateY);
        }
        if(!is_dir($upload_path."/".$dateY."/".$dateM)){
            mkdir($upload_path."/".$dateY."/".$dateM);
        }
         
        $upload_path .= $dateY."/".$dateM."/";
        $upload_url = base_url().$upload_path;

        // Upload file. 

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 3000,
            'max_height' => 3000,
            'file_name' => intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('image')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $data = array('address' => $address,
                          'sale_type' => $sale_type, 
                          'listing_type' => $listing_type, 
                          'acquistition_type' => $acquistition_type, 
                          'title' => $title, 
                          'area' => $area, 
                          'user_id' => $user_id, 
                          'bed_count' => $bed_count, 
                          'bath_count' => $bath_count, 
                          'price' => $price, 
                          'description' => $description, 
                          'image_url' => $file_url, 
                          'created_at' => date('Y-m-d h:m:s')
                          );
            $id = $this->api_model->createProperty($data);
            $result['image_url'] = $file_url;  
            $info = $this->api_model->getPropertyOwnerInfo($id);
            $message = $info['user_name']." added new property ".$info['title']." on ".date('M d Y');
            $data = array('content' => $message, 
                          'created_at' => date('Y-m-d h:m:s'));
            $this->api_model->addNotification($data);          
            $this->doRespondSuccess($result);

        } else {

            $this->doRespond(206, $result);// upload fail
            return;
        }
    }
    
    function editProperty() {
        
        $result = array();
        
        $id = $_POST['id'];
        $address = $_POST['address'];
        $title = $_POST['title'];
        $area = $_POST['area'];
        $listing_type = $_POST['listing_type'];
        $acquistition_type = $_POST['acquistition_type'];
        $sale_type = $_POST['sale_type'];
        $bed_count = $_POST['bed_count'];
        $bath_count = $_POST['bath_count'];
        $price = $_POST['price'];
        $description = $_POST['description'];
        
        if (empty($_FILES['image'])) {
            
            $data = array('address' => $address,
                          'sale_type' => $sale_type, 
                          'listing_type' => $listing_type, 
                          'acquistition_type' => $acquistition_type,
                          'title' => $title, 
                          'area' => $area, 
                          'bed_count' => $bed_count, 
                          'bath_count' => $bath_count, 
                          'price' => $price, 
                          'description' => $description,                           
                          );
            $this->api_model->editProperty($id, $data);
            $info = $this->api_model->getPropertyOwnerInfo($id);
            $message = $info['user_name']." changed property ".$info['title']." on ".date('M d Y');
            $data = array('content' => $message, 
                          'created_at' => date('Y-m-d h:m:s'));
            $this->api_model->addNotification($data);
            $this->doRespondSuccess($result);            
            
        } else {
            
            if(!is_dir("uploadfiles/")) {
                mkdir("uploadfiles/");
            }
            $upload_path = "uploadfiles/";  

            $cur_time = time();
             
            $dateY = date("Y", $cur_time);
            $dateM = date("m", $cur_time);
             
            if(!is_dir($upload_path."/".$dateY)){
                mkdir($upload_path."/".$dateY);
            }
            if(!is_dir($upload_path."/".$dateY."/".$dateM)){
                mkdir($upload_path."/".$dateY."/".$dateM);
            }
             
            $upload_path .= $dateY."/".$dateM."/";
            $upload_url = base_url().$upload_path;

            // Upload file. 

            $w_uploadConfig = array(
                'upload_path' => $upload_path,
                'upload_url' => $upload_url,
                'allowed_types' => "*",
                'overwrite' => TRUE,
                'max_size' => "100000KB",
                'max_width' => 3000,
                'max_height' => 3000,
                'file_name' => intval(microtime(true) * 10)
            );

            $this->load->library('upload', $w_uploadConfig);

            if ($this->upload->do_upload('image')) {

                $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
                $data = array('address' => $address,
                              'sale_type' => $sale_type,
                              'listing_type' => $listing_type, 
                              'acquistition_type' => $acquistition_type, 
                              'bed_count' => $bed_count, 
                              'area' => $area, 
                              'bath_count' => $bath_count, 
                              'title' => $title,
                              'price' => $price, 
                              'description' => $description, 
                              'image_url' => $file_url,
                              );
                $this->api_model->editProperty($id, $data);
                $info = $this->api_model->getPropertyOwnerInfo($id);
                $message = $info['user_name']." changed property ".$info['title']." on ".date('M d Y');
                $data = array('content' => $message, 
                              'created_at' => date('Y-m-d h:m:s'));
                $this->api_model->addNotification($data);
                $result['image_url'] = $file_url;            
                $this->doRespondSuccess($result);

            } else {

                $this->doRespond(206, $result);// upload fail
                return;
            }
            
        }
    }
    
    function deleteProperty() {
        
        $result = array();        
        $id = $_POST['id'];        
        
        $info = $this->api_model->getPropertyOwnerInfo($id);
        $message = $info['user_name']." removed his property ".$info['title']." on ".date('M d Y');
        $data = array('content' => $message, 
                      'created_at' => date('Y-m-d h:m:s'));
        $this->api_model->addNotification($data); 
        $this->api_model->deleteProperty($id);       
        $this->doRespondSuccess($result);
    }
    
    function soldProperty() {
        $result = array();        
        $id = $_POST['id'];        
        $this->api_model->soldProperty($id);        
        $this->doRespondSuccess($result);
    }
    
    function getAllPropertyList() {
        
        $result = array();
        
        $result['property_list'] = $this->api_model->getAllPropertyList();
        
        $this->doRespondSuccess($result);  
          
    }
    
    function updateEmail() {
        
        $result = array();
        $user_id = $_POST['user_id'];
        $email = $_POST['email'];
        
        $this->api_model->updateEmail($user_id, $email);
        
        $this->doRespondSuccess($result);
        
    }
    
    function updatePhone() {
        
        $result = array();
        $user_id = $_POST['user_id'];
        $phone = $_POST['phone'];
        
        $this->api_model->updatePhone($user_id, $phone);
        
        $this->doRespondSuccess($result);        
    }
    
    function addShortList() {
        
        $result = array();
        $user_id = $_POST['user_id'];
        $property_id = $_POST['property_id'];
        $data = array('user_id' => $user_id, 'property_id' => $property_id);
        
        $this->api_model->addShortList($data);
        $this->doRespondSuccess($result);
        
    }
    
    function getPropertyList() {
        
        $result = array();
        $user_id = $_POST['user_id'];        
        $result['property_list'] = $this->api_model->getPropertyList($user_id);        
        $this->doRespondSuccess($result);  
        
    }
    
    function getShortList() {
        
        $result = array();
        $user_id = $_POST['user_id'];        
        $result['property_list'] = $this->api_model->getShortList($user_id);        
        $this->doRespondSuccess($result);
    } 
    
    function uploadPhoto() {
         
         $result = array();
         
         $user_id = $_POST['user_id'];

                  
         if(!is_dir("uploadfiles/")) {
             mkdir("uploadfiles/");
         }
         $upload_path = "uploadfiles/";  

         $cur_time = time();
         
         $dateY = date("Y", $cur_time);
         $dateM = date("m", $cur_time);
         
         if(!is_dir($upload_path."/".$dateY)){
             mkdir($upload_path."/".$dateY);
         }
         if(!is_dir($upload_path."/".$dateY."/".$dateM)){
             mkdir($upload_path."/".$dateY."/".$dateM);
         }
         
         $upload_path .= $dateY."/".$dateM."/";
         $upload_url = base_url().$upload_path;

        // Upload file. 

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 3000,
            'max_height' => 4000,
            'file_name' => $user_id.'_'.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('photo')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $data = array('photo_url' => $file_url, 
                          'updated_at' => date('Y-m-d h:m:s')
                          );
            $id = $this->api_model->upload_photo($user_id, $data);
            $result['photo_url'] = $file_url;            
            $this->doRespondSuccess($result);

        } else {

            $this->doRespond(206, $result);// upload fail
            return;
        }     
    }
    
    function resetPropertyList() {
        $result = array();
        $user_id = $_POST['user_id'];
        $this->api_model->resetPropertyList($user_id);
        $this->doRespondSuccess($result);
    }
    
    function removeShortList() {
        
        $result = array();
        $user_id = $_POST['user_id'];
        $property_id = $_POST['property_id'];
        $data = array('user_id' => $user_id, 'property_id' => $property_id);
        
        $this->api_model->removeShortList($data);
        $this->doRespondSuccess($result);
        
    }
    
    function resetPassword() {
        
        $result = array();
        $user_id = $_POST['user_id'];
        $email = $_POST['email'];
        $old_pwd = $_POST['old_password'];
        $new_pwd = $_POST['new_password'];
        
        if ($this->api_model->checkEmail($user_id, $email)) {
            
            if ($this->api_model->resetPassword($user_id, $old_pwd, $new_pwd)) {
                $this->doRespondSuccess($result);
            } else {
                $result['message'] = "Wrong old password";
                $this->doRespond(203, $result);
            }
            
        } else {
            $result['message'] = "User with email does not exist";
            $this->doRespond(202, $result);
        }
    }
    
    function searchProperty() {
        
        $result = array();
        $area = $_POST['area'];
        $keyword = $_POST['keyword'];
        $type = $_POST['sale_type'];
        $listing_type = $_POST['listing_type'];
        $acquistition_type = $_POST['acquistition_type'];
        $min_price = $_POST['min_price'];
        $max_price = $_POST['max_price'];
        $bed_count = $_POST['bed_count'];
        $bath_count = $_POST['bath_count'];
        
        $data = array('area' => $area,
                      'keyword' => $keyword,
                      'sale_type' => $type,
                      'listing_type' => $listing_type, 
                      'acquistition_type' => $acquistition_type,
                      'min_price' => $min_price,
                      'max_price' => $max_price,
                      'bed_count' => $bed_count,
                      'bath_count' => $bath_count,
                      );
                      
        $result['data'] = $data;
        
        $result['property_list'] = $this->api_model->searchProperty($data);
        
        $this->doRespondSuccess($result);
        
        
    }
    
    function getEvent() {
        $result = array();
        $result['event_list'] = $this->api_model->getEvent();
        $this->doRespondSuccess($result);
    }                                  
}
?>
